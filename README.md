Simple Snake
============

I found some of my old Java code lurking on a CD backup.

Thought I'd have a go at porting it over to Javascript.

This is pretty much how I structured it in Java, not to be confused with best practice!

Use the arrow keys to move your snake, try to eat the green plants as quickly as you can while avoiding everything else.

If you die, click in the play area to reset.

## License

Simple Snake is available under the MIT license. See the LICENSE file for more info.