/**
 * Rescued some old Java code from a CD.
 *
 * No idea why I created it in the first place, but ported to Javascript
 *
 * This is pretty much how I structured it in Java, not to be confused
 * with best practice!
 */
(function (canvas, undefined) {

    var game, runloop, lastkey, renderer;

    renderer = function (canvas) {

        var scale, size, ctx,
            backColour = '#cccccc',
            snakeColour = '#0000aa',
            goalColour = '#00aa00',
            wallColour = '#000000',
            obstacleColour = '#aa00aa',
            deadColour = '#aa0000';

        scale = Math.max(Math.min(Math.floor(canvas.width / 40), Math.floor(canvas.height / 20)), 5);
        size = scale - 2;
        ctx = canvas.getContext('2d');

        /**
         * Private function to draw the circles
         * @param x
         * @param y
         * @param w
         * @param c
         */
        var fillCircle = function (x, y, w, c) {
            var radius = Math.floor(w / 2);
            ctx.fillStyle = c;
            ctx.beginPath();
            ctx.arc(x + radius, y + radius, radius, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();
        };

        /**
         * Return our public methods
         */
        return {
            'block': function (row, col, value) {

                var x, y;
                x = (col * scale) + 1;
                y = (row * scale) + 1;

                ctx.fillStyle = backColour;
                ctx.fillRect(x, y, scale, scale);

                switch (value) {
                    case 'g':
                        fillCircle(x, y, size, goalColour);
                        break;
                    case 'o':
                        fillCircle(x, y, size, obstacleColour);
                        break;
                    case 's':
                        fillCircle(x, y, size, snakeColour);
                        break;
                    case 'd':
                        fillCircle(x, y, size, deadColour);
                        break;
                    case 'w':
                        ctx.fillStyle = wallColour;
                        ctx.fillRect(x, y, size, size);
                        break;
                }
            },
            'points': function (points) {

                ctx.fillStyle = backColour;
                ctx.fillRect(scale, 0, scale * 5, scale);
                ctx.fillStyle = wallColour;
                ctx.font = "20px Impact";
                ctx.fillText(points, scale + 2, scale - 2);
            },
            'reset': function () {

                ctx.fillStyle = backColour;
                ctx.fillRect(0, 0, canvas.width, canvas.height);
            }
        }

    }(canvas);

    game = function (render) {

        var store = [];
        var snake = [];
        var points = 0;
        var goalPoints = 50;
        var that = this;

        /**
         * Private function to return a random number restricted to
         * a given range
         * @param min
         * @param max
         * @returns {int}
         */
        var getRandomInt = function (min, max) {

            return Math.floor(Math.random() * (max - min + 1)) + min;
        };

        /**
         * Return our public methods
         */
        return {

            /**
             * Initialise and build our game area
             */
            'build': function () {

                var row, col, rand;

                /**
                 * Store for our game area
                 * @type {Array}
                 */
                store = [];
                /**
                 * Store for our snake
                 * @type {Array}
                 */
                snake = [];

                /**
                 * Initialise the display
                 */
                render.reset();

                /**
                 * Reset points
                 */
                points = 0;

                /**
                 * Build the game area
                 */
                for (row = 0; row < 20; row++) {

                    rand = getRandomInt(1, 38);
                    for (col = 0; col < 40; col++) {

                        if (row == 0 || row == 19 || col == 0 || col == 39) {

                            this.update(row, col, 'w');
                        }
                        else if (col == rand) {

                            this.update(row, col, 'o');
                        }
                    }
                }

                /**
                 * Add the snake and a little breathing room
                 */
                this.update(4, 13, 's');
                this.update(4, 14, 's');
                this.update(4, 15, 's');
                this.update(4, 16, 's');
                this.update(4, 17, false);
                this.update(4, 18, false);
                this.update(4, 19, false);
                this.update(4, 20, false);

                /**
                 * Add something for us to reach
                 */
                this.addGoal();

                /**
                 * Initialise the points display
                 */
                render.points(points);
            },
            /**
             * Pick a random position for the goal making
             * sure the position is not already occupied
             */
            'addGoal': function () {

                var row, col;

                col = getRandomInt(1, 38);
                row = getRandomInt(1, 19);

                if (this.get(row, col) === false) {

                    this.update(row, col, 'g');
                }
                else {

                    this.addGoal();
                }
            },
            /**
             * Will return the status of a square in the game area,
             * returning false if nothing present
             * @param row
             * @param col
             * @returns {*}
             */
            'get': function (row, col) {

                if (store[row] && store[row][col]) {

                    return store[row][col];
                }
                return false;
            },
            /**
             * Update a square in the game area, calling the
             * renderer for display if it has changed
             * @param row
             * @param col
             * @param value
             */
            'update': function (row, col, value) {

                var oldValue;

                if (!store[row]) {

                    store[row] = [];
                }

                oldValue = this.get(row, col);

                if (oldValue != value) {

                    store[row][col] = value;
                    render.block(row, col, value);

                    if (value === 's') {

                        snake.push([row, col]);
                    }
                }
            },
            /**
             * Kill the snake
             */
            'die': function () {

                var that = this;
                snake.forEach(function (pos) {
                    that.update(pos[0], pos[1], 'd');
                });
            },
            /**
             * Main game loop, will move the snake in the direction
             * last selected (lastkey), checking to see what it hits
             * If we return false then the snake has died and the game
             * has ended.
             * @param lastkey
             * @returns {boolean}
             */
            'move': function (lastkey) {

                var head = snake.slice(-1)[0];
                var row = head[0];
                var col = head[1];
                var tail;

                /**
                 * Check the direction of the snake
                 */
                switch (lastkey) {
                    case 38:
                        row--;
                        break;
                    case 40:
                        row++;
                        break;
                    case 37:
                        col--;
                        break;
                    default:
                        col++;
                        break;
                }

                /**
                 * Depending on the contents of the square the snake has
                 * entered, either kill it or let it on its way
                 */
                switch (game.get(row, col)) {

                    case 'g':
                        /**
                         * Snake has reached a goal, increment the point total
                         * and spawn another goal
                         */
                        points += goalPoints;
                        render.points(points);
                        game.update(row, col, 's');
                        game.addGoal();
                        goalPoints = 50;
                        break;

                    case 'w':
                    case 'o':
                    case 's':
                        /**
                         * Snake has hit something it shouldn't, return
                         * false to signal end of game
                         */
                        this.die();
                        return false;
                        break;

                    default:
                        /**
                         * Snake can enter the new square
                         */
                        game.update(row, col, 's');
                        /**
                         * Remove the old tail
                         */
                        tail = snake.shift();
                        game.update(tail[0], tail[1], false);
                        /**
                         * Decremnent the points so you get less points the
                         * longer it takes you to reach a goal
                         */
                        goalPoints = (Math.max(1, goalPoints - 1));
                        break;
                }

                /**
                 * Return true to signal that the game is still running
                 */
                return true;
            }
        }
    }(renderer);

    function init() {

        /**
         * Start as if we selected the right key
         * @type {number}
         */
        lastkey = 39;

        /**
         * Set up our game
         */
        game.build();

        /**
         * Start everything moving
         * @type {number}
         */
        runloop = setInterval(function () {
            if (!game.move(lastkey)) {
                clearInterval(runloop);
                runloop = false;
            }
        }, 180);

        /**
         * Check for keypresses and keep track of the last one
         * @param e
         */
        document.onkeydown = function (e) {
            if (e.keyCode >= 37 && e.keyCode <= 40) {
                lastkey = e.keyCode;
            }
        };

    }

    /**
     * If we are passed a HTMLCanvasElement, then we are good to go
     */
    if (canvas instanceof HTMLCanvasElement) {

        init();

        /**
         * Clicking in the canvas will reset the game if it has
         * stopped
         */
        canvas.onclick = function () {
            if (!runloop) {
                init();
            }
        };
    }
}(document.getElementById('snake')));
